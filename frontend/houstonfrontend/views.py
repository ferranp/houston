
from django.shortcuts import render
import simplejson as json
import pygraphviz as pgv
import tempfile
import os
import httplib
from django.http import HttpResponse

def index(request):

    conn = httplib.HTTPConnection("localhost:8080")
    conn.request("GET", "/")

    r1 = conn.getresponse()
    data1 = r1.read()
    conn.close()
    data = json.loads(data1)
    return render(request, 'houstonfrontend/index.html', data)

def system(request, system):

    conn = httplib.HTTPConnection("localhost:8080")
    conn.request("GET", "/%s" % system)

    r1 = conn.getresponse()
    data1 = r1.read()
    conn.close()
    data = { 'system': json.loads(data1) }
    return render(request, 'houstonfrontend/system.html', data)

def system_graph(request, system):

    conn = httplib.HTTPConnection("localhost:8080")
    conn.request("GET", "/%s" % system)

    r1 = conn.getresponse()
    data1 = r1.read()
    conn.close()
    system = json.loads(data1)

    G=pgv.AGraph(directed=True)

    for node in system['states']:
        if node == system['state']:
            G.add_node(node, color="red")
        else:
            G.add_node(node)


    for edge in system['transitions']:
        G.add_edge(edge['from'],edge['to'], edge['name'], label=edge['name'])

    fd, fname = tempfile.mkstemp(suffix=".png")
    os.close(fd)
    G.layout()
    G.draw(fname)

    img = open(fname,'rb').read()

    return HttpResponse(img, mimetype="image/png")


