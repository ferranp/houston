#!/usr/bin/env python
import sys
import logging
log = logging.getLogger(__name__)

from houston import Container
from houston.system import System
from houston import http
from houston import transition

def main():

    format = "%(asctime)s %(levelname)-10s %(message)s"
    logging.basicConfig(format=format, level=logging.DEBUG,stream=sys.stdout)

    container = Container()

    http.run(container)

if __name__ == '__main__': # pragma: no cover
    main()

