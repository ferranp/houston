
from system import System, Event
from wsgiref.simple_server import make_server, WSGIRequestHandler
from bottle import Bottle, request, debug
import simplejson as json



def run(container, host='localhost', port=8080):
    class QuietHandler(WSGIRequestHandler):
        def log_request(*args, **kw): pass

    app = Bottle()
    debug(True)
    @app.route('/')
    def index():
        systems = container.systems.keys()
        data = {
            'systems': systems
        }
        return json.dumps(data, indent=2)

    @app.post('/')
    def add_system():
        data = request.body.read()
        data = json.loads(data)
        container.add_system(System(**data))
        return "CREATED"

    @app.route('/:system')
    def system_data(system):
        stm = container[system]
        transitions = [ t.as_dict() for t in stm.transitions]
        events = set()
        for t in transitions:
            for event in t['triggers']:
                events.add(event)

        data = {
            'name': stm.name,
            'state': stm.current_state.name,
            'states': stm.states,
            'transitions': transitions,
            'events': list(events),
            'actions': stm.actions,
        }
        return json.dumps(data, indent=2)

    @app.route('/:system/state')
    def system_state(system):
        stm = container[system]
        data = {
            'state': stm.current_state.name,
        }
        return json.dumps(data, indent=2)

    @app.post('/:system/event')
    def index(system):
        stm = container[system]
        event = request.POST['event']
        stm.events.append(Event(event))
        return 'CREATED' + event + "\n"

    srv = make_server(host, port, app, handler_class = QuietHandler)
    srv.serve_forever()


