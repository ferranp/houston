#!/usr/bin/env python
import threading
from datetime import datetime
import time
import sys
import logging
import os
import conditions
import transition

class Event(object):
    def __init__(self, event, data=None):
        self.name = event
        self.data = data

DEFAULT_TIMEOUT = 1

class System(threading.Thread):

    def __init__(self, name, state, states, transitions, \
                    actions=None, loop_timeout=DEFAULT_TIMEOUT):
        super(System,self).__init__()

        self.name = name
        self.log = logging.getLogger(name)

        assert state in states

        self.last_transition = None
        self.last_trigger = None

        self.states = states
        if actions is None:
            actions = {}
        self.actions = actions
        self.transitions = transition.load_transitions(transitions)
        self.events = []

        self.timeout = loop_timeout
        self.current_state = State(self, state)



    def process_event(self, event):
        self.log.debug("Processing event " + event.name)
        for transition in self.transitions:

            if transition.from_ != self.current_state.name:
               continue

            if event.name not in transition.triggers:
               continue
            if transition.check_conditions(self.current_state):
                self.set_state(State(self, transition.to), transition, event)
                return

    def process_state(self):
        for transition in self.transitions:
            if transition.from_ != self.current_state.name:
                continue

            if transition.triggers:
               continue

            if transition.check_conditions(self.current_state):
               self.set_state(State(self, transition.to), transition)
               return

    def set_state(self, state, transition, event=None):
        self.current_state = state
        self.last_transition = transition
        self.last_trigger = event

        self.process_state()


    def run(self):
        self.process_state()
        while True:
            # wait for events or timout
            event = self.wait_for_event(self.timeout)
            if event:
                self.log.debug("Process event :" + event.name)
                self.process_event(event)
            else:
                self.log.debug("Process state")
                self.process_state()

    def wait_for_event(self, timeout):
          time.sleep(timeout)
          if self.events:
             return self.events.pop()
          else:
             return None



class State(object):
    def __init__(self, system, name, event=None):
        system.log.debug("State: " + name)
        self.system = system
        self.name = name
        self.tms = datetime.now()
        self.event = event
        self.run_actions()


    def run_actions(self):
        if self.name in self.system.actions:
            for action in self.system.actions[self.name]:
                self.run_action(action)

    def run_action(self, action):
        self.system.log.info("Running command : %s" % str(action))

        type_ = action['type']
        del action['type']
        if type_ == 'cmd':
            self.run_action_cmd(**action)
        elif type_ == 'event':
            self.run_action_event(**action)

    def run_action_cmd(self, cmd, **kwargs):
        # TODO subprocess
        os.system(cmd)

    def run_action_event(self, event, data, **kwargs):
        self.system.events.append(Event(event, data))



