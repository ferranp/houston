
from datetime import datetime, timedelta
import logging
log = logging.getLogger(__name__)

class Condition(object):

    def check(self, state):
        raise NotImplemented


class TrueCondition(Condition):

    def check(self, state):
        return True

    def __str__(self):
        return "true"

class FalseCondition(Condition):

    def check(self, state):
        return False

    def __str__(self):
        return "false"

class NotCondition(Condition):
    def __init__(self, condition):
       self.condition = condition

    def check(self,state):
        return not self.condition(state)

    def __str__(self):
        return "not(%s)" % self.condition

class TimeoutCondition(Condition):

    def __init__(self, timeout):
        self.timeout = timeout

    def check(self, state):
        if datetime.now() > (state.tms + timedelta(seconds=self.timeout)):
           return True
        else:
           return False

    def __str__(self):
        return "timeout(%d)" % self.timeout

class CalendarCondition(Condition):

    def check(self, state):
        if self.check_in_calendar(now()):
           return True
        else:
           return False


class WeekendCondition(CalendarCondition):
    def check_in_calendar(date):
        return date.weekday in (5,6)

    def __str__(self):
        return "weekend"


class WeekdayCondition(CalendarCondition):
    def check_in_calendar(date):
        return date.weekday not in (5,6)

    def __str__(self):
        return "weekday"


class WorkhoursCondition(CalendarCondition):
    def check_in_calendar(date):
        return date.hour in (8,9,10,11,12,13,14,15,16,17,18)

    def __str__(self):
        return "workhours"


CONDITIONS = {
    'true': TrueCondition(),
    'false': FalseCondition(),
    'weekend': WeekendCondition(),
    'weekday': WeekdayCondition,
    'workours': WorkhoursCondition(),
}


def load_condition(cnd):
    if cnd in CONDITIONS:
        return CONDITIONS[cnd]

    if cnd.startswith('not'):
        c = load_condition(cnd[3:-1])
        return NotCondition(c)

    if cnd.startswith('timeout'):
        return TimeoutCondition(int(cnd[8:-1]))


def load_conditions(data):
    conditions = []
    for cnd in data:
        conditions.append(load_condition(cnd))
    return conditions

