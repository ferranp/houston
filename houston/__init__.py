#!/usr/bin/env python
import threading
from datetime import datetime
import time
import sys
import logging
log = logging.getLogger(__name__)

import conditions
import transition
import http

class Container(object):

    def __init__(self, systems=None):
        self.systems = {}
        if systems:
            for system in systems:
                self.systems[system.name] = system

    def __getitem__(self, key):
        return self.systems[key]

    def __setitem__(self, key, value):
        self.systems[key] = value


    def add_system(self, system):
        self[system.name] = system
        system.daemon = True
        system.start()
