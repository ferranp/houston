import logging
log = logging.getLogger(__name__)

import conditions

class Transition(object):
    def __init__(self, name, from_, to, triggers, conditions):
       self.name = name
       self.from_ = from_
       self.to = to
       self.triggers = triggers
       self.conditions = conditions

    def check_conditions(self, state):
        for condition in self.conditions:
            log.info("Checing condition %s" % condition)
            if not condition.check(state):
               return False

        return True

    def as_dict(self):
        return {
            'name':  self.name,
            'from':  self.from_,
            'to':  self.to,
            'triggers':  self.triggers,
            'conditions':  [str(c) for c in self.conditions],
        }


def load_transitions(data):
    transitions = []
    for trans in data:
        triggers = trans['triggers']
        conds = conditions.load_conditions(trans['conditions'])
        t = Transition(trans['name'], trans['from'], trans['to'], triggers, conds)
        transitions.append(t)

    return transitions
